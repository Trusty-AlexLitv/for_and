const { Router } = require('express');
const { getInitMw } = require('libs/auth');
const facebookRouter = require('./facebook');

const router = Router();


router.use(getInitMw());

router.use('/facebook', facebookRouter);

module.exports = router;
