const { Router } = require('express');
const strategy = require('libs/auth').strategy.facebook;

const router = Router();
const { getLoginMw, getCallbackMw } = strategy;

router.get('/', getLoginMw());

router.get('/callback', getCallbackMw(), (req, res) => {
  console.log('req.user:', req.user);
  res.send({ status: 'ok' });
});

module.exports = router;
