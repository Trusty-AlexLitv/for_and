const fs = require('fs');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const privateKeyPath = require('config').get('auth:keys:pub');
// const publicKeyPath = require('config').get('auth:keys:priv');
const issuer = require('config').get('app:name');

const facebookStrategy = require('./strategies/facebook');

const privateKey = fs.readFileSync(privateKeyPath, 'utf-8');
// const publicKey = fs.readFileSync(publicKeyPath, 'utf-8');

const getPrivateKey = () => privateKey;

const getInitMw = () => passport.initialize();
const createJWT = (subject, user) => new Promise((resolve, reject) => {
  const signOptions = {
    issuer,
    subject,
    audience: 'users',
    algorithm: 'RS256',
  };

  jwt.sign({ user }, privateKey, signOptions, (err, token) => {
    if (err) {
      // console.log(err);
      reject(err);
    } else {
      resolve(token);
    }
  });
});

module.exports.createJWT = createJWT;
module.exports.getInitMw = getInitMw;
module.exports.getPrivateKey = getPrivateKey;
module.exports.strategy = {
  facebook: facebookStrategy,
};
