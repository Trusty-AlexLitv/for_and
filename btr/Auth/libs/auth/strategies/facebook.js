const passport = require('passport');
const { Strategy } = require('passport-facebook');

const clientId = require('config').get('auth:providers:facebook:clientId');
const clientSecret = require('config').get('auth:providers:facebook:clientSecret');
const callbackUrl = require('config').get('auth:providers:facebook:callbackUrl');
const scope = require('config').get('auth:providers:facebook:scope');

passport.use(new Strategy({
  clientID: clientId,
  clientSecret,
  callbackURL: callbackUrl,
},
((accessToken, refreshToken, payload, cb) => {
  const { id, displayName } = payload;

  const profile = {
    displayName,
    id,
    token: accessToken,
  };

  return cb(null, profile);
})));

passport.serializeUser((user, cb) => {
  console.log('uns', user);
  cb(null, user);
});

passport.deserializeUser((obj, cb) => {
  cb(null, obj);
});


const getLoginMw = () => passport.authenticate('facebook', {
  scope,
  session: false,
});

const getCallbackMw = () => passport.authenticate('facebook', { failureRedirect: '/login' });

module.exports.getLoginMw = getLoginMw;
module.exports.getCallbackMw = getCallbackMw;
