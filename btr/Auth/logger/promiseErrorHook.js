const { logger } = require('logger');

// Uncatched Promise error
const init = () => {
  process.on('unhandledRejection', (err) => {
    logger.error(err);
  });
};

module.exports = init;
