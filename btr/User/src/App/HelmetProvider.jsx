import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Helmet } from 'react-helmet';

const companyName = 'Starline Tours';

const HelmetProvider = (props) => {
  const { title, description } = props;
  return (
    <Helmet>
      <title>{`${companyName} | ${title}`}</title>
      <meta name="description" content={description} />
    </Helmet>
  );
};

HelmetProvider.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  title: state.head.title,
  description: state.head.description,
});

export default connect(mapStateToProps)(HelmetProvider);
