import React from 'react';
import MainPage from 'containers/MainPage';
import { BrowserRouter } from 'react-router-dom';

import store from 'store';
import { Provider } from 'react-redux';
import HelmetProvider from './HelmetProvider';

import 'normalize.css';
import './style.scss';

const App = () => (
  <Provider store={store}>
    <HelmetProvider />
    <BrowserRouter>
      <MainPage />
    </BrowserRouter>
  </Provider>
);

export default App;
