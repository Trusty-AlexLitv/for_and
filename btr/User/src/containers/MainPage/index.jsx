import React from 'react';
import MapBg from 'components/MapBg';
import Menu from 'components/Menu';
import Content from 'components/Content';
import Footer from 'components/Footer';
import style from './style.module.scss';


const MainPage = () => (
  <div className={style.root}>
    <div className={style.bg}>
      <MapBg />
    </div>
    <div className={style.page}>

      <div className={style.menu}>
        <Menu />
      </div>

      <div className={style.content}>
        <Content />
      </div>

      <div className={style.footer}>
        <Footer />
      </div>
    </div>
  </div>
);

export default MainPage;
