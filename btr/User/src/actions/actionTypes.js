/* eslint-disable import/prefer-default-export */
export const APP_ONLINE = 'APP_ONLINE';
export const APP_OFFLINE = 'APP_OFFLINE';

export const HEAD_SET = 'HEAD_SET';

export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';

export const MAIN_MENU_TOGGLE = 'MAIN_MENU_TOGGLE';
export const MAIN_MENU_CLOSE = 'MAIN_MENU_CLOSE';

export const MAP_SET_USER_POS = 'MAP_SET_USER_POS';
