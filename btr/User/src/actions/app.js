import { APP_ONLINE, APP_OFFLINE } from 'actions/actionTypes';

const offline = (payload) => {
  const action = {
    type: APP_ONLINE,
    payload,
  };

  return action;
};

const online = (payload) => {
  const action = {
    type: APP_OFFLINE,
    payload,
  };

  return action;
};

export { offline, online };
export default null;
