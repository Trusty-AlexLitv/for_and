/* eslint-disable import/prefer-default-export */
import { 
  MAP_SET_USER_POS,
} from 'actions/actionTypes';

const setUserPos = (payload) => {
  const action = {
    type: MAP_SET_USER_POS,
    payload,
  };

  return action;
};

export {
  setUserPos,
};
