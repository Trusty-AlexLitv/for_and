import { MAIN_MENU_TOGGLE, MAIN_MENU_CLOSE } from 'actions/actionTypes';

const menuStatusToogle = () => {
  const action = {
    type: MAIN_MENU_TOGGLE,
  };

  return action;
};

const menuClose = () => {
  const action = {
    type: MAIN_MENU_CLOSE,
  };

  return action;
};

export { menuStatusToogle, menuClose };
export default null;
