import { HEAD_SET } from 'actions/actionTypes';

const setHead = (payload) => {
  const action = {
    type: HEAD_SET,
    payload,
  };

  return action;
};

export { setHead };
export default null;
