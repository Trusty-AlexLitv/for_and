import { APP_ONLINE, APP_OFFLINE } from 'actions/actionTypes';
import update from 'immutability-helper';

const initialState = {
  isOnline: true,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
  case APP_ONLINE:
    return update(state, { isOnline: { $set: true } });
  case APP_OFFLINE:
    return update(state, { isOnline: { $set: false } });
  default:
    return state;
  }
};

export default reducer;
