import { MAIN_MENU_TOGGLE, MAIN_MENU_CLOSE } from 'actions/actionTypes';
import update from 'immutability-helper';

const initialState = {
  open: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
  case MAIN_MENU_TOGGLE:
    return update(state, { open: { $set: !state.open } });
  case MAIN_MENU_CLOSE:
    return update(state, { open: { $set: false } });
  default:
    return state;
  }
};

export default reducer;
