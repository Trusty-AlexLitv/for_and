import { AUTH_SUCCESS, AUTH_LOGOUT } from 'actions/actionTypes';
import { update } from 'immutability-helper';

const initialState = {
  user: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
  case AUTH_SUCCESS:
    return update(state, { user: { $set: action.payload.id } });
  case AUTH_LOGOUT:
    return update(state, { user: { $set: null } });
  default:
    return state;
  }
};

export default reducer;
