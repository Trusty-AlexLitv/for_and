import { combineReducers } from 'redux';
import app from './app';
import auth from './auth';
import head from './head';
import mainMenu from './mainMenu';
import map from './map';

export default combineReducers({
  app,
  auth,
  head,
  mainMenu,
  map,
});
