import React from 'react';
import ReactMapboxGl, { Layer, Feature } from 'react-mapbox-gl';

const accessToken = 'pk.eyJ1IjoiYWxleGxpdHYiLCJhIjoiY2pwZXZldXFsMDQ0aDNwbGdvbWlram02eSJ9.j0g-j3pnvjXQPDYphzB-Nw';
const Map = ReactMapboxGl({
  accessToken,
});

const center = [-118.325279, 34.094669];

const coordinates1 = [
  [-122.48369693756104, 37.83381888486939],
  [-122.48348236083984, 37.83317489144141],
  [-122.48339653015138, 37.83270036637107],
  [-122.48356819152832, 37.832056363179625],
  [-122.48404026031496, 37.83114119107971],
  [-122.48404026031496, 37.83049717427869],
  [-122.48348236083984, 37.829920943955045],
  [-122.48356819152832, 37.82954808664175],
  [-122.48507022857666, 37.82944639795659],
  [-122.48610019683838, 37.82880236636284],
];
const coordinates2 = [
  [-122.48695850372314, 37.82931081282506],
  [-122.48700141906738, 37.83080223556934],
  [-122.48751640319824, 37.83168351665737],
  [-122.48803138732912, 37.832158048267786],
  [-122.48888969421387, 37.83297152392784],
  [-122.48987674713133, 37.83263257682617],
  [-122.49043464660643, 37.832937629287755],
  [-122.49125003814696, 37.832429207817725],
  [-122.49163627624512, 37.832564787218985],
  [-122.49223709106445, 37.83337825839438],
  [-122.49378204345702, 37.83368330777276],
];

const MapBg = () => (
  <Map
    // eslint-disable-next-line react/style-prop-object
    style="mapbox://styles/mapbox/light-v9"
    containerStyle={{
      height: '100%',
      width: '100%',
    }}
    center={center}
  >
    <Layer
      type="line"
      id="route"
      paint={{ 'line-width': 4 }}
    >
      <Feature coordinates={coordinates1} />
      <Feature coordinates={coordinates2} />
    </Layer>
  </Map>
);

export default MapBg;
