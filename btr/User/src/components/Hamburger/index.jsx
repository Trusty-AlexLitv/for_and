import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { menuStatusToogle } from 'actions/mainMenu';

import classNames from 'classnames';
import style from './style.module.scss';

const Hamburger = (props) => {
  const { open, handlerMenuStatusToogle } = props;

  const className = classNames(
    style.root,
    { [style.open]: open },
  );

  return (
    <div
      className={className}
      onClick={handlerMenuStatusToogle}
    >
      <span />
      <span />
      <span />
    </div>
  );
};

Hamburger.propTypes = {
  open: PropTypes.bool.isRequired,
  handlerMenuStatusToogle: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  open: state.mainMenu.open,
});

const mapDispatchToProps = dispatch => ({
  handlerMenuStatusToogle: () => dispatch(menuStatusToogle()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Hamburger);
