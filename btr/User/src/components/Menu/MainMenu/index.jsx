import React from 'react';
import { Link } from 'react-router-dom';

import style from './style.module.scss';

const MainMenu = () => (
  <div className={style.root}>
    <Link to="map">Map explore</Link>
    <Link to="offers">All offers</Link>
    <Link to="purchases">Purchases</Link>
    <Link to="payment">Payment</Link>
  </div>
);

export default MainMenu;
