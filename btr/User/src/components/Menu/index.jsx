import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { menuClose } from 'actions/mainMenu';
import { setHead } from 'actions/head';
import classNames from 'classnames';

import Hamburger from 'components/Hamburger';
import Search from './Search';
import MainMenu from './MainMenu';

import style from './style.module.scss';

class Menu extends Component {
  componentWillMount() {
    const { dispatcherSetHead } = this.props;
    const title = 'Map Explore';
    dispatcherSetHead({ title });
  }

  componentDidMount() {
    const matchMedia = window.matchMedia('(min-width: 576px)');
    const { dispatcherMenuClose } = this.props;

    matchMedia.addListener(dispatcherMenuClose);
  }

  getRootClassName = () => {
    const { isMenuOpen } = this.props;

    const className = classNames(
      style.root,
      { [style.open]: isMenuOpen },
    );

    return className;
  }

  render() {
    const { getRootClassName } = this;
    const { title } = this.props;

    const rootClassName = getRootClassName();

    return (
      <div className={rootClassName}>
        <div className={style.title}>
          <span>{title}</span>
          <div className={style.hamburger}>
            <Hamburger />
          </div>
        </div>
        <div className={style.search}>
          <Search />
        </div>
        <div className={style.mainMenu}>
          <MainMenu />
        </div>
      </div>
    );
  }
}

Menu.propTypes = {
  isMenuOpen: PropTypes.bool.isRequired,
  dispatcherMenuClose: PropTypes.func.isRequired,
  dispatcherSetHead: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  isMenuOpen: state.mainMenu.open,
  title: state.head.title,
});

const mapDispatchToProps = dispatch => ({
  dispatcherMenuClose: () => dispatch(menuClose()),
  dispatcherSetHead: data => dispatch(setHead(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
