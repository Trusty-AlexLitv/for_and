import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';

import style from './style.module.scss';

const Content = () => (
  <div className={style.root}>
    <Scrollbars
      style={{ height: '100%' }}
    >
      <div style={{ height: '9999px' }}>
        123
        <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle cx="11" cy="11" r="10.25" fill="#7447F9"/>
          <circle cx="11" cy="11" r="10.25" fill="url(#paint0_linear)"/>
          <circle cx="11" cy="11" r="10.25" stroke="white" stroke-width="0.5"/>
          <path d="M10.7389 15.3874C11.0021 16.1787 11.5014 16.2066 11.8537 15.4523L15.8582 6.87141C16.2104 6.11554 15.8838 5.78966 15.1288 6.14194L6.54804 10.1461C5.793 10.4984 5.82166 10.9978 6.61215 11.2618L9.70699 12.293L10.7389 15.3874Z" fill="white"/>
          <defs>
            <linearGradient id="paint0_linear" x1="11" y1="1" x2="11" y2="21" gradientUnits="userSpaceOnUse">
              <stop stop-color="#7447F9"/>
              <stop offset="1" stop-color="#5F29FF"/>
            </linearGradient>
          </defs>
        </svg>
      </div>
    </Scrollbars>
  </div>
);

export default Content;
