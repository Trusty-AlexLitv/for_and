const routeModel = require('models/route');

const createRoute = async (data) => {
  const doc = await routeModel.create(data);
  const { id } = doc;
  return id;
};

const getRoute = async () => {
  const docs = await routeModel.getAll();
  return docs;
};

module.exports.createRoute = createRoute;
module.exports.getRoute = getRoute;
