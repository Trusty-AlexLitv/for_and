const { Router } = require('express');

const router = Router();

router.get('/', (req, res) => {
  res.json({ status: 'ok', payload: 'Hello! I`m an api server. Read the docs for use' });
});

module.exports = router;
