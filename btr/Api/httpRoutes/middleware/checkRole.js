const createError = require('http-errors');
const { decode } = require('controllers/jwt');

const env = process.env.NODE_ENV;

const middleware = allowedRoles => (req, res, next) => {
  // test
  const token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwicm9sZXMiOlsiYWRtaW4iXSwiaWF0IjoxNTE2MjM5MDIyfQ.fUsHkcxn3Dr2xh2FYhVgkb7Zo7H-EPogmVXQiOOeMMgiya77yOK2X8u0TTqc8K50HoobdlXAnxUo_Xu8bqreFu1fuoRLE5xovzXOSvluVNHJ65DSJ9VQ_lJmUgLXjql502SCa9EoNkZDbKbIdD2HdJDbUEJ2nER_mSp6Uj7sJ4c';

  decode(token).then((data) => {
    const { status } = data;
    if(status !== 'ok') {
      
    }

    const check = roles.reduce((acc, val) => {
      if(acc) {
        return acc;
      }

      if( allowedRoles.indexOf(val) !== -1) {
        return true;
      };

      return acc;
    }, false);
  }).catch((err) => {
    if (env !== 'development') {
      next(createError(404));
      return;
    }

    next(createError(403, err));
  });
};

module.exports = middleware;
