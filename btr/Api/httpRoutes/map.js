const { Router } = require('express');
const Ajv = require('ajv');
const mapController = require('controllers/map');

const router = Router();
const ajv = new Ajv();
const checkRole = require('httpRoutes/middleware/checkRole');

const routeSchema = require('schemas/http/route');

router.get('/all', (req, res) => {
  const routes = [{
    color: '#519643',
    title: 'Green route',
    lines: [{
      thickness: 4,
      coordinates: [-118.24988544451315, 34.10212710573714],
    }, {
      thickness: 4,
      coordinates: [-118.24865990604434, 34.10084692964112],
    }, {
      thickness: 4,
      coordinates: [-118.24743594018324, 34.09905544847034],
    }, {
      thickness: 4,
      coordinates: [-118.24059070498998, 34.09193329974465],
    }, {
      thickness: 4,
      coordinates: [-118.25044355163483, 34.09341788628639],
    }],
  }];

  const stations = [{
    title: '1',
    coordinates: [-118.24988544451315, 34.10212710573714],
  }, {
    title: '2',
    coordinates: [-118.24059070498998, 34.09193329974465],
  }, {
    title: '3',
    coordinates: [-118.25044355163483, 34.09341788628639],
  }];

  const discountPlaces = [{
    id: '1',
    coordinates: [-118.25153656770948, 34.08752523217059],
  }];

  res.json({ status: 'ok', payload: { routes, stations, discountPlaces } });
});

router.post('/', (req, res) => {
  const data = res.body;

  const valid = ajv.validate(routeSchema, data);
  if (!valid) {
    const { errors } = ajv;

    const result = {
      status: 'invalid data',
      payload: { errors },
    };
    res.json(result);
    return;
  }

  res.json({ status: 'ok' });
});

router.get('/route', checkRole(['admin']), async (req, res) => {
  // const { route } = res.body.payload;

  const route = {
    color: '#519643',
    title: 'Green route',
    lines: [{
      thickness: 4,
      coordinates: [-118.24988544451315, 34.10212710573714],
    }, {
      thickness: 4,
      coordinates: [-118.24865990604434, 34.10084692964112],
    }, {
      thickness: 4,
      coordinates: [-118.24743594018324, 34.09905544847034],
    }, {
      thickness: 4,
      coordinates: [-118.24059070498998, 34.09193329974465, 34.09193329974465],
    }, {
      thickness: 4,
      coordinates: [-118.25044355163483, 34.09341788628639],
    }],
  };

  const validate = ajv.compile(routeSchema);
  const valid = validate(route);

  if (!valid) {
    const { errors } = validate;

    const result = {
      status: 'invalid data',
      payload: { errors },
    };
    res.json(result);
    return;
  }

  const { createRoute } = mapController;
  const id = await createRoute(route);

  res.json({ status: 'ok', payload: { id } });
});

module.exports = router;
