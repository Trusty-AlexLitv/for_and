const path = require('path');
const mongoose = require('mongoose');
const isHexcolor = require('is-hexcolor');

const { Schema } = mongoose;

const generalSchema = new Schema({
  color: {
    type: Schema.Types.String,
    default: '#cccccc',
  },
  title: {
    type: Schema.Types.String,
    default: 'Route',
    minLength: 1,
    maxLength: 255,
  },
  lines: [{
    thickness: {
      type: Schema.Types.Number,
      default: 4,
      min: 0.1,
      max: 10,
    },
    coordinates: {
      type: Schema.Types.Array,
      items: { type: Schema.Types.Number },
      minItems: 0,
      maxItems: 10,
    },
  }],
});

generalSchema.path('color').validate(
  isHexcolor,
  'Color `{VALUE}` not valid',
  'Invalid color',
);

const modelname = path.basename(__filename);
const model = mongoose.model(modelname, generalSchema);
module.exports = model;
