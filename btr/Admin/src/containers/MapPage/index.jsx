import React from 'react';

import Map from 'components/Map';
import Menu from 'components/Menu';
import style from './style.module.scss';

const MapPage = () => (
  <div className={style.root}>
    <div className={style.map}>
      <Map />
    </div>
    <div className={style.menu}>
      <Menu />
    </div>
  </div>
);

export default MapPage;
