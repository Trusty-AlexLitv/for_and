import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Link, Switch, Route, withRouter,
} from 'react-router-dom';

import MapPage from 'containers/MapPage';
import DiscountPage from 'containers/DiscountPage';
import Login from 'components/Login';
import style from './style.module.scss';

const MainPage = (props) => {
  const { isGuest } = props;

  if (isGuest) {
    return (
      <div className={style.loginPanel}>
        <Login />
      </div>
    );
  }

  return (
    // <Router>
    <div className={style.root}>
      <div className={style.menu}>
        <Link to="/map">Map</Link> | <Link to="/discounts">Discount</Link>
      </div>

      <Switch>
        <Route path="/map" exact component={MapPage} />
        <Route path="/discounts" exact component={DiscountPage} />
      </Switch>
    </div>
    // </Router>
  );
};

MainPage.propTypes = {
  isGuest: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  isGuest: state.auth.status !== 'authenticated',
});

// export default connect(mapStateToProps)(MainPage);
export default withRouter(connect(mapStateToProps)(MainPage));
