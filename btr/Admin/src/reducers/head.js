import { HEAD_SET } from 'actions/actionTypes';
import update from 'immutability-helper';

const initialState = {
  title: '',
  description: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
  case HEAD_SET:
    return update(state, { $merge: action.payload });
  default:
    return state;
  }
};

export default reducer;
