import { AUTH_SUCCESS, AUTH_IN_POCESSING, AUTH_LOGOUT } from 'actions/actionTypes';
import update from 'immutability-helper';

const initialState = {
  profile: {},
  token: null,
  status: 'guest',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
  case AUTH_SUCCESS:
    return update(state, {
      profile: { $set: action.payload.profile },
      token: { $set: action.payload.token },
      status: { $set: 'authenticated' },
    });
  case AUTH_IN_POCESSING:
    return update(state, {
      profile: { $set: {} },
      token: { $set: null },
      status: { $set: 'processing' },
    });
  case AUTH_LOGOUT:
    return update(state, {
      profile: { $set: {} },
      token: { $set: null },
      status: { $set: 'guest' },
    });
  default:
    return state;
  }
};

export default reducer;
