import {
 MAP_SET_ALL_BASIC, MAP_ACTIVE_SET, MAP_ACTIVE_RESET, MAP_MOUSE_COORDINATE 
} from 'actions/actionTypes';
import update from 'immutability-helper';

const initialState = {
  routes: [{
    color: '#519643',
    title: 'Green route',
    lines: [{
      thickness: 4,
      coordinates: [-118.24988544451315, 34.10212710573714],
    }, {
      thickness: 4,
      coordinates: [-118.24865990604434, 34.10084692964112],
    }, {
      thickness: 4,
      coordinates: [-118.24743594018324, 34.09905544847034],
    }, {
      thickness: 4,
      coordinates: [-118.24059070498998, 34.09193329974465],
    }, {
      thickness: 4,
      coordinates: [-118.25044355163483, 34.09341788628639],
    }],
    stations: [{
      title: 1,
      coordinates: [-118.24988544451315, 34.10212710573714],
    }, {
      title: 2,
      coordinates: [-118.24059070498998, 34.09193329974465],
    }, {
      title: 3,
      coordinates: [-118.25044355163483, 34.09341788628639],
    }],
  }],
  user: {
    coordinates: [-122.48369693756104, 37.83381888486939],
    angle: 90,
  },
  active: {},
  discountPlaces: [{
    id: '1',
    coordinates: [-118.25153656770948, 34.08752523217059],
  }],
  mouseCoordinate: { lng: 0, lat: 0 },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
  case MAP_SET_ALL_BASIC:
    return update(state, {
      routes: { $set: action.payload.routes },
      // stations: { $set: action.payload.stations },
      discountPlaces: { $set: action.payload.discountPlaces },
    });
  case MAP_ACTIVE_SET:
    return update(state, {
      active: { $set: action.payload },
    });
  case MAP_ACTIVE_RESET:
    return update(state, {
      active: { $set: {} },
    });
  case MAP_MOUSE_COORDINATE:
    return update(state, {
      mouseCoordinate: { $set: { lng: action.payload.lng, lat: action.payload.lat } },
    });
  default:
    return state;
  }
};

export default reducer;
