/* eslint-disable import/prefer-default-export */
import { AUTH_SUCCESS, AUTH_LOGOUT } from 'actions/actionTypes';

const login = (jwt) => {
  const profile = atob(jwt.split('.')[1]); // get and decode profile in jwt

  const payload = {
    profile,
    token: jwt,
  };

  const action = {
    type: AUTH_SUCCESS,
    payload,
  };

  return action;
};

const loguot = () => ({
  type: AUTH_LOGOUT,
});


export { login, loguot };
