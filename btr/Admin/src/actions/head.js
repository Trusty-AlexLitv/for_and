/* eslint-disable import/prefer-default-export */
import { HEAD_SET } from 'actions/actionTypes';

const setHead = (payload) => {
  const action = {
    type: HEAD_SET,
    payload,
  };

  return action;
};

export { setHead };
