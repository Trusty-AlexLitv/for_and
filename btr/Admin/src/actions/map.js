/* eslint-disable import/prefer-default-export */
import {
  MAP_SET_ALL_BASIC,
  MAP_ACTIVE_SET,
  MAP_ACTIVE_RESET,
  MAP_MODE_CREATE_ROUTE,
  MAP_MOUSE_COORDINATE,
  MAP_ROUTE_CHANGE_PARAMS,
  MAP_ROUTE_CHANGE_LINE,
} from 'actions/actionTypes';

const setActive = (payload) => {
  const action = {
    type: MAP_ACTIVE_SET,
    payload,
  };

  return action;
};

const resetActive = () => {
  const action = {
    type: MAP_ACTIVE_RESET,
  };

  return action;
};

const setModeCreateRoute = () => {
  const action = {
    type: MAP_MODE_CREATE_ROUTE,
  };

  return action;
};

const setMouseCoordinate = (payload) => {
  const action = {
    type: MAP_MOUSE_COORDINATE,
    payload,
  };

  return action;
};

const setRouteParams = (payload) => {
  const action = {
    type: MAP_ROUTE_CHANGE_PARAMS,
    payload,
  };

  return action;
};

const setRouteLine = (payload) => {
  const action = {
    type: MAP_ROUTE_CHANGE_LINE,
    payload,
  };

  return action;
};

const loadData = (payload) => {
  const action = {
    type: MAP_SET_ALL_BASIC,
    payload,
  };

  return action;
};

export {
  setActive, resetActive, setModeCreateRoute, setMouseCoordinate, setRouteParams, setRouteLine, loadData,
};
