/* eslint-disable import/prefer-default-export */
export const APP_ONLINE = 'APP_ONLINE';
export const APP_OFFLINE = 'APP_OFFLINE';

export const HEAD_SET = 'HEAD_SET';

export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_IN_POCESSING = 'AUTH_IN_POCESSING';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';

export const MAIN_MENU_TOGGLE = 'MAIN_MENU_TOGGLE';
export const MAIN_MENU_CLOSE = 'MAIN_MENU_CLOSE';

export const MAP_SET_ALL_BASIC = 'MAP_SET_ALL_BASIC';

export const MAP_ACTIVE_SET = 'MAP_ACTIVE_SET';
export const MAP_ACTIVE_RESET = 'MAP_ACTIVE_RESET';

export const MAP_MOUSE_COORDINATE = 'MAP_MOUSE_COORDINATE';

export const MAP_MODE_CREATE_ROUTE = 'MAP_MODE_CREATE_ROUTE';

export const MAP_ROUTE_CHANGE_PARAMS = 'MAP_ROUTE_CHANGE_PARAMS';
export const MAP_ROUTE_CHANGE_LINE = 'MAP_ROUTE_CHANGE_LINE';
