import React, { Component } from 'react';
import PropTypes from 'prop-types';

import style from './style.module.scss';

class BusStation extends Component {
  handleLogin = () => {

  }

  render() {
    const { title } = this.props;

    return (
      <div className={style.root}>
        <div className={style.ico}>
          <svg viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="10" cy="10" r="8.5" stroke="#519643" strokeWidth="3" />
          </svg>
        </div>
        <div className={style.title}>
          {title}
        </div>
      </div>
    );
  }
}

BusStation.propTypes = {
  title: PropTypes.number.isRequired,
};

export default BusStation;
