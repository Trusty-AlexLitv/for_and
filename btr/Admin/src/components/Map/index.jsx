import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  setActive as setActiveAction,
  setMouseCoordinate as setMouseCoordinateAction,
  loadData as loadDataAction,
} from 'actions/map';

import axios from 'axios';

import CirclePoint from 'components/CirclePoint';
import BusStation from 'components/BusStation';

import ReactMapboxGl, { Layer, Feature, Marker } from 'react-mapbox-gl';

import style from './style.module.scss';

const accessToken = 'pk.eyJ1IjoiYWxleGxpdHYiLCJhIjoiY2pwZXZldXFsMDQ0aDNwbGdvbWlram02eSJ9.j0g-j3pnvjXQPDYphzB-Nw';
const MapGL = ReactMapboxGl({
  accessToken,
});

const center = [-118.325279, 34.094669];

class Map extends Component {
  // constructor(props) {
  // super(props);
  // const { geolocation } = navigator;
  // geolocation.getCurrentPosition((pos) =>{
  //   const { latitude: lat, longitude: lng } = pos.coords;
  //   console.log(lat, lng);
  // });
  // }

  componentDidMount() {
    const { loadData } = this.props;
    loadData();
  }

  handleMouseMove = (map, ev) => {
    const lng = Number(ev.lngLat.lng);
    const lat = Number(ev.lngLat.lat);
    const { setMouseCoordinate } = this.props;

    setMouseCoordinate({ lng, lat });
  }

  handleClickRouteLine = (ev, routeIdx, lineIdx) => {
    const { setActive } = this.props;
    setActive({
      routeIdx: Number(routeIdx),
      lineIdx: Number(lineIdx),
      type: 'line',
    });
  }

  handleMouseEnterLine = (ev, routeIdx, lineIdx) => {
    const { map } = ev;
    const { setActive, active } = this.props;

    // eslint-disable-next-line no-shadow
    const { style } = map.getCanvas();

    style.cursor = 'pointer';

    if (!(active && active.routeIdx === routeIdx && active.type === 'line')) {
      return;
    }

    const payload = {
      routeIdx,
      lineIdx,
      type: 'line',
    };

    setActive(payload);
  }

  handleMouseLeaveLine = (ev) => {
    const { map } = ev;
    // eslint-disable-next-line no-shadow
    const { style } = map.getCanvas();

    style.cursor = 'crosshair';
  }

  handleOnLoad = (map) => {
    // eslint-disable-next-line no-shadow
    const { style } = map.getCanvas();
    style.cursor = 'crosshair';
  }

  getLineColor = (routeColor, active, routeIdx, lineIdx) => {
    const { type: activeType } = active;
    if (activeType !== 'line') {
      return routeColor;
    }

    const {
      routeIdx: activeRouteIdx,
      lineIdx: activeLineIdx,
    } = active;

    if (activeRouteIdx !== routeIdx) {
      return routeColor;
    }

    if (activeLineIdx === lineIdx) {
      return '#cccccc';
    }

    return '#000000';
  }

  render() {
    const {
      handleClickRouteLine,
      handleMouseEnterLine,
      handleMouseLeaveLine,
      handleOnLoad,
      handleMouseMove,
      getLineColor,
    } = this;
    const { routes, discountPlaces, active } = this.props;

    return (
      <MapGL
        // eslint-disable-next-line react/style-prop-object
        style="mapbox://styles/mapbox/light-v9"
        center={center}
        className={style.root}
        onStyleLoad={handleOnLoad}
        onMouseMove={handleMouseMove}
      >
        {routes.map((route, routeIdx) => route.lines.map((line, lineIdx) => (
          <Layer
            key={line.coordinates.toString()}
            type="line"
            paint={{
              // 'line-color': active.type === 'line' && active.routeIdx === routeIdx ? '#000000' : route.color,
              'line-color': getLineColor(route.color, active, routeIdx, lineIdx),
              'line-width': line.thickness,
            }}
          >
            <Feature
              coordinates={[line.coordinates, route.lines[lineIdx + 1] ? route.lines[lineIdx + 1].coordinates : route.lines[0].coordinates]}
              className={style.line}
              onClick={ev => handleClickRouteLine(ev, routeIdx, lineIdx)}
              onMouseEnter={ev => handleMouseEnterLine(ev, routeIdx, lineIdx)}
              onMouseLeave={handleMouseLeaveLine}
            />
          </Layer>
        )))}

        {routes[0].stations.map((point, key) => (
          <Marker
            key={key}
            coordinates={point.coordinates}
          >
            <div className={style.station}>
              <BusStation title={point.title} />
            </div>
          </Marker>
        ))}

        {discountPlaces.map((point, key) => (
          <Marker
            key={key}
            coordinates={point.coordinates}
          >
            <div className={style.station}>
              <CirclePoint />
            </div>
          </Marker>
        ))}
      </MapGL>
    );
  }
}

Map.propTypes = {
  setActive: PropTypes.func.isRequired,
  setMouseCoordinate: PropTypes.func.isRequired,
  // active: PropTypes.shape({

  // }).isRequired,
  active: PropTypes.object.isRequired,
  routes: PropTypes.array.isRequired,
  discountPlaces: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
  active: state.map.active,
  routes: state.map.routes,
  discountPlaces: state.map.discountPlaces,
});

const mapDispatchToProps = dispatch => ({
  setActive: payload => dispatch(setActiveAction(payload)),
  setMouseCoordinate: payload => dispatch(setMouseCoordinateAction(payload)),
  loadData: async () => {
    const { data } = await axios('/map/all');
    const { payload } = data;
    dispatch(loadDataAction(payload));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Map);
