import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames';
import style from './style.module.scss';
import { setRouteParams as setRouteParamsAction, setRouteLine as setRouteLineAction } from 'actions/map';


class LineTools extends Component {
  handleChangeParams = (ev) => {
    const { setRouteParams } = this.props;
  }

  handleChangeLine = (ev) => {
    const { setRouteLine } = this.props;
  }

  render() {
    const {
      route,
      lineIdx,
    } = this.props;
    console.log(route);
    console.log(lineIdx);

    return (
      <div className={style.root}>
        <div>
          <br />color: <input type="text" name="color" value={route.color} />
          <br />title: <input type="text" name="color" value={route.title} />
        </div>
        <div>
          <hr />
          {route.lines.map((line, idx) => (
            <div key={idx} className={classNames({ [style.activeLine]: idx === lineIdx })}>
              <div>
                <br />lng: <input type="number" name="color" value={line.coordinates[0]} />
                <br />lat: <input type="number" name="color" value={line.coordinates[1]} />
              </div>
              <br />Line# {idx}
              <br />thickness: <input type="number" name="color" value={line.thickness} />
            </div>
          ))}
          <div>
            <br />closure to start point
            <div>
              <br />lng: {route.lines[0].coordinates[0]}
              <br />lat: {route.lines[0].coordinates[1]}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

LineTools.propTypes = {
  route: PropTypes.object.isRequired,
  lineIdx: PropTypes.number.isRequired,
  setRouteParams: PropTypes.func.isRequired,
  setRouteLine: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  const { active } = state.map;

  return {
    route: state.map.routes[active.routeIdx],
    lineIdx: active.lineIdx,
  };
};

const mapDispatchToProps = dispatch => ({
  setRouteParams: dispatch(setRouteParamsAction()),
  setRouteLine: dispatch(setRouteLineAction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(LineTools);
