import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loguot as loguotAction } from 'actions/auth';
import { Scrollbars } from 'react-custom-scrollbars';

import LineTools from './LineTools';

import style from './style.module.scss';

class Menu extends Component {
  test = () => {

  }

  render() {
    const {
      userName,
      loguot,
      activeType,
      mouseCoordinate,
    } = this.props;

    console.log(activeType);

    return (
      <div className={style.root}>
        <div className={style.title}>
          <span>{userName}</span>
          <button
            type="button"
            onClick={loguot}
          >
            Loguot
          </button>
        </div>

        <div className={style.mouseCoordinate}>
          <div>Mouse coordinate</div>
          <br />lng: {mouseCoordinate.lng}
          <br />lat: {mouseCoordinate.lat}
        </div>

        <div className={style.paramsDesk}>
          <Scrollbars
            style={{ height: '100%' }}
          >
            {activeType === 'line' ? (
              <LineTools />
            ) : null}
          </Scrollbars>
        </div>
      </div>
    );
  }
}

Menu.propTypes = {
  loguot: PropTypes.func.isRequired,
  userName: PropTypes.string,
  activeType: PropTypes.string.isRequired,
  mouseCoordinate: PropTypes.object.isRequired,
};

Menu.defaultProps = {
  userName: '',
};

const mapStateToProps = state => ({
  userName: state.auth.profile.name,
  activeType: state.map.active.type ? state.map.active.type : '',
  mouseCoordinate: state.map.mouseCoordinate,
});

const mapDispatchToProps = dispatch => ({
  loguot: () => dispatch(loguotAction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
