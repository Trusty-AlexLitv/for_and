import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { login as loginAction } from 'actions/auth';
import { withRouter } from 'react-router-dom';


import style from './style.module.scss';

class Login extends Component {
  handleLogin = () => {
    const { login, history } = this.props;
    history.push('/map');

    // for test
    const jwt = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSm9obiBEb2UiLCJyb2xlcyI6WyJ1c2VyIiwiYWRtaW4iXSwiaWF0IjoxNTE2MjM5MDIyfQ.s83dE4Lp0AtZJbTog3iJjImszh5pM_uPg4kWkwTAFj6IQgT_wNqngVr_iGAIFHT6_YAxkM19F_26M13U64QkNkBBFtgfgngSv9HTX3fNJum4zfh8EzrVe5Lpq5FeWt9anAhv9zky5Xydnmt90Kd8hqK5xBis6-iQ7chMiESonfE';
    login(jwt);
  }

  render() {
    const { handleLogin } = this;

    return (
      <div className={style.root}>
        <button
          type="button"
          onClick={handleLogin}
        >
          Login
        </button>
      </div>
    );
  }
}

Login.propTypes = {
  login: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  history: PropTypes.object.isRequired,
};

const mapDispatchToProps = dispatch => ({
  login: jwt => dispatch(loginAction(jwt)),
});

export default withRouter(connect(null, mapDispatchToProps)(Login));
