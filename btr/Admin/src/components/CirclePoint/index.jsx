import React, { Component } from 'react';
import style from './style.module.scss';

class CirclePoint extends Component {
  handleLogin = () => {

  }

  render() {
    return (
      <div className={style.root}>
        <svg width="10" height="10" viewBox="0 0 10 10" xmlns="http://www.w3.org/2000/svg">
          <circle cx="5" cy="5" r="3.5" stroke="#519643" strokeWidth="3" />
        </svg>
      </div>
    );
  }
}

export default CirclePoint;
