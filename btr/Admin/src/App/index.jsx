import React from 'react';
import MainPage from 'containers/MainPage';
import { BrowserRouter as Router } from 'react-router-dom';
import store from 'store';
import { Provider } from 'react-redux';
import HelmetProvider from './HelmetProvider';

import 'normalize.css';
import './style.scss';

const App = () => (
  <Provider store={store}>
    <HelmetProvider />
    <Router>
      <MainPage />
    </Router>
  </Provider>
);

export default App;
